#!/usr/bin/env python3
# -*- coding:utf-8 -*-
###
# Copyright (c) 2022 Haofan Zheng
# Use of this source code is governed by an MIT-style
# license that can be found in the LICENSE file or at
# https://opensource.org/licenses/MIT.
###


from DHomeHTTPSServer.Client.SSLContext import (
	GetAuthClientContext
)
from DHomeHTTPSServer.Client import Request
from DHomeHTTPSServer.Client import ReqBodyJson


def main():
	cltCtx = GetAuthClientContext(
		clientCertPath='./test-credentials/clt.secp384r1.crt',
		clientKeyPath ='./test-credentials/clt.secp384r1.priv',
		caCertPath    ='./test-credentials/ca.secp384r1.crt'
	)
	reqBody = {
		'domain': 'MyEmailServer.com',
		'to': [
			'test-receiver1@example.com',
		],
		'bcc': [
			'test-receiver2@example.com',
		],
		'subject': 'Test Email',
		'content': 'This is a test email.',
		'attachments': [
			{
				'data': 'SGVsbG8sIFdvcmxkIQ==', # b'Hello, World!'
				'maintype': 'text',
				'subtype': 'plain',
				'filename': 'hello.txt'
			}
		],
	}
	respRaw, respType, respParsed = Request.PostRequest(
		cltCtx,
		'localhost',
		54321,
		'/send_email/',
		ReqBodyJson.ReqBodyJson(reqBody)
	)

	if respType != 'json':
		raise ValueError('Unexpected response type: ' + respType)
	if respParsed['success'] != True:
		raise ValueError('Unexpected response: ' + str(respParsed))

	print('Email sent successfully.')


if __name__ == '__main__':
	main()
