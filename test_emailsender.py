#!/usr/bin/env python3
# -*- coding:utf-8 -*-
###
# Copyright (c) 2022 Haofan Zheng
# Use of this source code is governed by an MIT-style
# license that can be found in the LICENSE file or at
# https://opensource.org/licenses/MIT.
###


import argparse
import json
import logging
import sys

from EmailSender import EmailSender


def LoadConfig(filePath: str) -> dict:
	with open(filePath, 'r') as f:
		return json.loads(f.read())


def SetupLogger(logLevel: str, logFile = None, logStream = None):

	logLevel = getattr(logging, logLevel.upper())

	logging.root.setLevel(logLevel)
	logFormatter = logging.Formatter(
		fmt='%(asctime)s [%(levelname)s]%(name)s - %(message)s',
		datefmt='%Y-%m-%dT%H:%M:%S%z',
	)

	if logStream is not None:
		logHdlr = logging.StreamHandler(logStream)
		logHdlr.setFormatter(logFormatter)
		logging.root.addHandler(logHdlr)

	if logFile is not None:
		logHdlr = logging.FileHandler(logFile)
		logHdlr.setFormatter(logFormatter)
		logging.root.addHandler(logHdlr)

	if logFile is not None:
		logging.root.info('Log will be written to file {}'.format(logFile))


def TestDKIMSignature():
	import dkim
	import DKIMKeyGenerator

	from EmailSender import EmailMsg
	from EmailSender import DKIMKeyMgr

	TEST_KEY_PARAMS = [
		('ed25519', 'ed25519-sha256'),
		('rsa', 'rsa-sha256'),
	]

	for keyType, sigAlg in TEST_KEY_PARAMS:
		dkimKey, dnsRec = DKIMKeyGenerator.GenerateKey(
			keyType=keyType
		)

		def TestDnsFunc(domain, timeout):
			domain = domain.decode('ascii')
			return dnsRec

		dkimKeyMgr = DKIMKeyMgr.DKIMKeyMgr(
			domain='MyEmailDomain.com',
			algorithm=sigAlg,
			selector=keyType,
			keyBytes=dkimKey.encode()
		)

		signTestEmail = EmailMsg.ConstructEmailMsgNoBcc(
			hdrFrom='test-sender1@example.com',
			hdrTo=[ 'test-receiver1@example.com' ],
			hdrSubject='Test email',
			content='This is a test email.'
		)

		signature = dkimKeyMgr.SignEmailMsg(signTestEmail)
		signTestEmail.add_header('DKIM-Signature', signature)

		vrfyRes = dkim.verify(
			message=signTestEmail.as_bytes(),
			dnsfunc=TestDnsFunc
		)
		assert(vrfyRes == True)


def main():
	parser = argparse.ArgumentParser(description='DHomeMessenger')
	parser.add_argument(
		'--config',
		type=str,
		help='Path to the config file.',
		required=True
	)
	args = parser.parse_args()

	# Read config
	config = LoadConfig(args.config)

	# Setup logger
	SetupLogger(
		logLevel=config['logLevel'],
		logFile=config['logFile'],
		logStream=sys.stdout
	)

	# Setup email sender
	emailSender = EmailSender.EmailSender(
		emailDomain=config['emailSender']['emailDomain'],
		cltKeyPath=config['emailSender']['cltKeyPath'],
		cltCertPath=config['emailSender']['cltCertPath'],
		dkimKeys=config['emailSender']['dkimKeys'],
		dryRun=config['dryRun']
	)

	emailSender.SendEmail(
		fromUsername='test-user',
		hdrTo=[ 'test1@gmail.com', 'test2@gmail.com' ],
		hdrSubject='Test email',
		content='This is a test email.'
	)

	emailSender.SendEmail(
		fromUsername='test-user',
		hdrTo=[ ],
		hdrBcc=[ 'test1@gmail.com', 'test2@gmail.com' ],
		hdrSubject='Test email',
		content='This is a test email.'
	)

	TestDKIMSignature()


if __name__ == '__main__':
	main()
