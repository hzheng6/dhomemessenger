#!/usr/bin/env python3
# -*- coding:utf-8 -*-
###
# Copyright (c) 2022 Haofan Zheng
# Use of this source code is governed by an MIT-style
# license that can be found in the LICENSE file or at
# https://opensource.org/licenses/MIT.
###


import argparse
import base64
from typing import Tuple, Union

from cryptography.hazmat.primitives.serialization import (
	Encoding,
	PrivateFormat,
	PublicFormat,
	NoEncryption
)


def GenEd25519(
) -> Tuple[
	str, # private key
	str, # p value (public key)
	str, # k value
]:

	from cryptography.hazmat.primitives.asymmetric.ed25519 import (
		Ed25519PrivateKey
	)

	privKey = Ed25519PrivateKey.generate()

	priv = privKey.private_bytes(
		encoding=Encoding.Raw,
		format  =PrivateFormat.Raw,
		encryption_algorithm=NoEncryption(),
	)
	priv = base64.b64encode(priv).decode()

	pub = privKey.public_key().public_bytes(
		encoding=Encoding.Raw,
		format  =PublicFormat.Raw,
	)
	pub = base64.b64encode(pub).decode()

	return (priv, pub, 'ed25519')


def GenRsa(
) -> Tuple[
	str, # private key
	str, # p value (public key)
	str, # k value
]:

	from cryptography.hazmat.primitives.asymmetric.rsa import (
		generate_private_key,
		RSAPrivateKey
	)

	privKey: RSAPrivateKey =  generate_private_key(
			public_exponent=65537,
			key_size=2048
	)

	priv = privKey.private_bytes(
		encoding=Encoding.PEM,
		format  =PrivateFormat.TraditionalOpenSSL,
		encryption_algorithm=NoEncryption(),
	).decode()

	pub = privKey.public_key().public_bytes(
		encoding=Encoding.DER,
		format  =PublicFormat.SubjectPublicKeyInfo,
	)
	pub = base64.b64encode(pub).decode()

	return (priv, pub, 'rsa')


GENERATOR_MAP = {
	'ed25519': GenEd25519,
	'rsa': GenRsa,
}


def GenerateKey(
	keyType: str,
	outputPath: Union[str, None] = None,
) -> Union[
	str, # DNS TXT record
	Tuple[str, str] # Tuple[private key, DNS TXT record]
]:

	priv, pVal, kVal = GENERATOR_MAP[keyType]()

	dnsTxtRec = 'v={}; k={}; p={};'.format(
		'DKIM1',
		kVal,
		pVal
	)

	if outputPath is None:
		return (priv, dnsTxtRec)
	else:
		with open(outputPath, 'w') as f:
			f.write(priv)

		return dnsTxtRec


def main():
	parser = argparse.ArgumentParser(description='DKIM key generator')
	parser.add_argument(
		'--key-type',
		type=str,
		help='Type of the key to generate (i.e., `rsa` or `ed25519`)',
		required=True,
		choices=GENERATOR_MAP.keys()
	)
	parser.add_argument(
		'--output',
		type=str,
		help='Path to the output file.',
		required=True
	)
	args = parser.parse_args()

	dnsTxtRec = GenerateKey(
		keyType=args.key_type,
		outputPath=args.output
	)

	print(dnsTxtRec)


if __name__ == '__main__':
	main()
