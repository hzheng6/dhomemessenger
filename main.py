#!/usr/bin/env python3
# -*- coding:utf-8 -*-
###
# Copyright (c) 2022 Haofan Zheng
# Use of this source code is governed by an MIT-style
# license that can be found in the LICENSE file or at
# https://opensource.org/licenses/MIT.
###


import argparse
import json
import logging
import sys
import threading
import time

from typing import Tuple

import WaitForInterrupt

from DHomeHTTPSServer.Server.UriUtils import SubUri as ServerSubUri
from DHomeHTTPSServer.Server.HttpReqHandler import HttpReqHandler
from DHomeHTTPSServer.Server.SSLContext import (
	GetAuthServerContext
)
from DHomeHTTPSServer.Server.HttpsServer import ThreadingHttpsServer
from DHomeHTTPSServer.Server.SimpleRateLimiter import SimpleRateLimiter

from EmailSender import EmailSender

import SiteMessenger.Singletons
import SiteMessenger.Urls


def LoadConfig(filePath: str) -> dict:
	with open(filePath, 'r') as f:
		return json.loads(f.read())


def SetupLogger(logLevel: str, logFile = None, logStream = None):

	logLevel = getattr(logging, logLevel.upper())

	logging.root.setLevel(logLevel)
	logFormatter = logging.Formatter(
		fmt='%(asctime)s [%(levelname)s]%(name)s - %(message)s',
		datefmt='%Y-%m-%dT%H:%M:%S%z',
	)

	if logStream is not None:
		logHdlr = logging.StreamHandler(logStream)
		logHdlr.setFormatter(logFormatter)
		logging.root.addHandler(logHdlr)

	if logFile is not None:
		logHdlr = logging.FileHandler(logFile)
		logHdlr.setFormatter(logFormatter)
		logging.root.addHandler(logHdlr)

	if logFile is not None:
		logging.root.info('Log will be written to file {}'.format(logFile))


def SetupAndRunService(
	config: dict
) -> Tuple[ThreadingHttpsServer, threading.Thread]:

	# Setup email sender
	emailSender = EmailSender.EmailSender(
		emailDomain=config['emailSender']['emailDomain'],
		cltKeyPath=config['emailSender']['cltKeyPath'],
		cltCertPath=config['emailSender']['cltCertPath'],
		dkimKeys=config['emailSender']['dkimKeys'],
		dryRun=config['dryRun']
	)

	SiteMessenger.Singletons.Init(
		emailSender=emailSender
	)

	svrHdl = ServerSubUri(SiteMessenger.Urls)

	svrAuth = GetAuthServerContext(
		serverCertPath=config['messengerServer']['certPath'],
		serverKeyPath=config['messengerServer']['keyPath'],
		caCertPath=config['messengerServer']['caCertPath']
	)

	# initialize the servers
	httpSvr = ThreadingHttpsServer(
		server_address=(
			config['messengerServer']['IP'],
			config['messengerServer']['port']
		),
		requestHandlerClass=HttpReqHandler,
		sslContext=svrAuth,
		uriHandlerMap={'*': svrHdl},
		rateLimiter=SimpleRateLimiter(6, 30.0),
		enabledMethods=('GET', 'POST',)
	)
	httpSvrThread = threading.Thread(
		target=httpSvr.serve_forever
	)
	httpSvrThread.start()

	return httpSvr, httpSvrThread


def main():
	parser = argparse.ArgumentParser(description='DHomeMessenger')
	parser.add_argument(
		'--config',
		type=str,
		help='Path to the config file.',
		required=True
	)
	args = parser.parse_args()

	# Read config
	config = LoadConfig(args.config)

	# Setup logger
	SetupLogger(
		logLevel=config['logLevel'],
		logFile=config['logFile'],
		logStream=sys.stdout
	)
	logger = logging.getLogger(__name__ + '.' + main.__name__)

	rebootSec = config['rebootSec']
	lastBootTime = time.time()

	waitForInterrupt = WaitForInterrupt.WaitForInterrupt()
	waitForInterrupt.RegisterHandler()

	httpSvr, httpSvrThread = None, None

	while not waitForInterrupt.IsSignalReceived():
		if httpSvr is None and httpSvrThread is None:
			# initial setup and run service
			httpSvr, httpSvrThread = SetupAndRunService(config)
			lastBootTime = time.time()
		elif time.time() - lastBootTime > rebootSec:
			# time to reboot
			logger.info('Rebooting service...')
			httpSvr.shutdown()
			httpSvrThread.join()
			httpSvr, httpSvrThread = None, None
		elif not httpSvrThread.is_alive():
			# service thread is dead
			logger.error('Service thread is dead, rebooting...')
			httpSvr, httpSvrThread = None, None

		time.sleep(waitForInterrupt.updInterval)

	if httpSvrThread is not None and httpSvrThread.is_alive():
		logger.info('Shutting down service...')
		if httpSvr is not None:
			httpSvr.shutdown()
		httpSvrThread.join()


if __name__ == '__main__':
	main()
