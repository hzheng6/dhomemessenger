#!/usr/bin/env python3
# -*- coding:utf-8 -*-
###
# Copyright (c) 2022 Haofan Zheng
# Use of this source code is governed by an MIT-style
# license that can be found in the LICENSE file or at
# https://opensource.org/licenses/MIT.
###


import base64
import email.message

from typing import List, Union


def ConstructEmailMsgNoBcc(
	hdrFrom: str,
	hdrTo: List[str],
	content: str,
	hdrSubject: Union[str, None] = None,
	attachments: Union[dict, None] = None,
) -> email.message.EmailMessage:

	if len(hdrTo) == 0:
		raise ValueError('No recipient specified.')

	hdrSubject = (
		hdrSubject
			if hdrSubject is not None
			else '(No Subject)'
	)

	emailMsg = email.message.EmailMessage()
	emailMsg['Subject'] = hdrSubject
	emailMsg['From'] = hdrFrom
	emailMsg['To'] = ', '.join(hdrTo)

	emailMsg.set_content(content)

	if attachments is not None:
		for item in attachments:
			attBin = base64.b64decode(item['data'].encode())
			emailMsg.add_attachment(
				attBin,
				maintype=item['maintype'],
				subtype=item['subtype'],
				filename=item['filename']
			)

	return emailMsg
