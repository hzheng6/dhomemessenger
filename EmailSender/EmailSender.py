#!/usr/bin/env python3
# -*- coding:utf-8 -*-
###
# Copyright (c) 2022 Haofan Zheng
# Use of this source code is governed by an MIT-style
# license that can be found in the LICENSE file or at
# https://opensource.org/licenses/MIT.
###


import email.message
import smtplib
import logging
import ssl
import os

from typing import List, Tuple, Union

from . import DKIMKeyMgr
from . import EmailMsg
from . import Utils


class EmailSender(object):

	def __init__(
		self,
		emailDomain: str,
		cltKeyPath: Union[str, os.PathLike],
		cltCertPath: Union[str, os.PathLike],
		dkimKeys: List[dict],
		dryRun: bool
	) -> None:
		super(EmailSender, self).__init__()

		self.emailDomain = emailDomain
		self.cltKeyPath = cltKeyPath
		self.cltCertPath = cltCertPath

		self.dkimKeyMgrs: List[DKIMKeyMgr.DKIMKeyMgr] = []
		for dkimKeyCfg in dkimKeys:
			self.dkimKeyMgrs.append(
				DKIMKeyMgr.DKIMKeyMgr(
					domain=self.emailDomain,
					**dkimKeyCfg
				)
			)

		self.dryRun = dryRun

		self.sslCtx = ssl.create_default_context(
			ssl.Purpose.SERVER_AUTH
		)
		self.sslCtx.minimum_version = ssl.TLSVersion.TLSv1_2
		self.sslCtx.verify_mode = ssl.CERT_REQUIRED
		self.sslCtx.check_hostname = True
		self.sslCtx.load_cert_chain(self.cltCertPath, self.cltKeyPath)
		self.sslCtx.load_default_certs()

		self.logger = logging.getLogger(
			__name__ + '.' + self.__class__.__name__
		)


	def _SendToOneRecp(
		self,
		senderAddr : str,
		recpAddr : str,
		emailMsg : email.message.EmailMessage
	) -> Union[dict, None]:

		self.logger.info(
			'Sending email from {} to {}'.format(senderAddr, recpAddr)
		)

		# Get target host name
		targetHostName = Utils.GetTargetHost(recpAddr)

		self.logger.debug(
			'Target mail server hostname is {}'.format(targetHostName)
		)

		### 4. Setup connection and send email
		if self.dryRun:
			self.logger.warn('DryRun mode, no email sent.')
			self.logger.debug('Email message:\n{}'.format(
				emailMsg.as_string()
			))
		else:
			targetHost = smtplib.SMTP(targetHostName)

			try:
				targetHost.starttls(context=self.sslCtx)

				sendRes = targetHost.sendmail(
					from_addr=senderAddr,
					to_addrs=recpAddr,
					msg=emailMsg.as_string()
				)

				if len(sendRes) > 0:
					self.logger.info(
						'Email sent from {} to {} with error: {}'.format(
							senderAddr,
							recpAddr,
							sendRes
						)
					)
					return sendRes
				else:
					self.logger.debug(
						'Sent email from {} to {}'.format(
							senderAddr,
							recpAddr
						)
					)
					return None
			finally:
				targetHost.quit()


	def _SignEmail(
		self,
		emailMsg: email.message.EmailMessage
	):
		signatures = []
		for dkimKeyMgr in self.dkimKeyMgrs:
			signRes = dkimKeyMgr.SignEmailMsg(emailMsg)
			signatures.append(signRes)

		for signature in signatures:
			emailMsg.add_header('DKIM-Signature', signature)


	def SendEmail(
		self,
		fromUsername : str,
		hdrTo: List[str],
		content: str,
		hdrBcc: Union[List[str], None] = None,
		hdrSubject: Union[str, None] = None,
		attachments: Union[dict, None] = None,
	) -> Union[
		List[Tuple[str, dict]],
		None
	]:
		hdrFrom = '{}@{}'.format(fromUsername, self.emailDomain)
		res = []

		# send 'to' and 'cc' recipients first
		if (hdrTo is not None) and (len(hdrTo) > 0):
			nonBccEmailMsg = EmailMsg.ConstructEmailMsgNoBcc(
				hdrFrom=hdrFrom,
				hdrTo=hdrTo,
				content=content,
				hdrSubject=hdrSubject,
				attachments=attachments
			)
			self._SignEmail(nonBccEmailMsg)
			for recpAddr in hdrTo:
				sendRes = self._SendToOneRecp(
					senderAddr=hdrFrom,
					recpAddr=recpAddr,
					emailMsg=nonBccEmailMsg
				)
				if sendRes is not None:
					res.append((recpAddr, sendRes))

		# send 'bcc' recipients
		if (hdrBcc is not None) and (len(hdrBcc) > 0):
			if (hdrTo is None) or (len(hdrTo) == 0):
				hdrTo = [ 'undisclosed-recipients:;' ]
			for recpAddr in hdrBcc:
				bccEmailMsg = EmailMsg.ConstructEmailMsgNoBcc(
					hdrFrom=hdrFrom,
					hdrTo=hdrTo,
					content=content,
					hdrSubject=hdrSubject,
					attachments=attachments
				)
				bccEmailMsg.add_header('Bcc', recpAddr)
				self._SignEmail(bccEmailMsg)
				sendRes = self._SendToOneRecp(
					senderAddr=hdrFrom,
					recpAddr=recpAddr,
					emailMsg=bccEmailMsg
				)
				if sendRes is not None:
					res.append((recpAddr, sendRes))

		if len(res) > 0:
			return res
		else:
			return None
