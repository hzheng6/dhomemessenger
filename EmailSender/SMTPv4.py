#!/usr/bin/env python3
# -*- coding:utf-8 -*-
###
# Copyright (c) 2022 Haofan Zheng
# Use of this source code is governed by an MIT-style
# license that can be found in the LICENSE file or at
# https://opensource.org/licenses/MIT.
###

# This is a modified version of the original smtplib.SMTP class
# The original class is licensed under the Python Software Foundation License
# https://docs.python.org/3/license.html


import smtplib
import socket


def create_connection_af(
	address,
	timeout=socket._GLOBAL_DEFAULT_TIMEOUT,
	source_address=None,
	family=0
):
	"""Connect to *address* and return the socket object.
	Convenience function.  Connect to *address* (a 2-tuple ``(host,
	port)``) and return the socket object.  Passing the optional
	*timeout* parameter will set the timeout on the socket instance
	before attempting to connect.  If no *timeout* is supplied, the
	global default timeout setting returned by :func:`getdefaulttimeout`
	is used.  If *source_address* is set it must be a tuple of (host, port)
	for the socket to bind as a source address before making the connection.
	A host of '' or port 0 tells the OS to use the default.
	"""

	host, port = address
	err = None
	for res in socket.getaddrinfo(host, port, family, socket.SOCK_STREAM):
		af, socktype, proto, canonname, sa = res
		sock = None
		try:
			sock = socket.socket(af, socktype, proto)
			if timeout is not socket._GLOBAL_DEFAULT_TIMEOUT:
				sock.settimeout(timeout)
			if source_address:
				sock.bind(source_address)
			sock.connect(sa)
			# Break explicitly a reference cycle
			err = None
			return sock

		except OSError as _:
			err = _
			if sock is not None:
				sock.close()

	if err is not None:
		try:
			raise err
		finally:
			# Break explicitly a reference cycle
			err = None
	else:
		raise OSError("getaddrinfo returns an empty list")


class SMTPv4(smtplib.SMTP):

	def _get_socket(self, host, port, timeout):
		# This makes it simpler for SMTP_SSL to use the SMTP connect code
		# and just alter the socket connection bit.
		if timeout is not None and not timeout:
			raise ValueError('Non-blocking socket (timeout=0) is not supported')
		if self.debuglevel > 0:
			self._print_debug('connect: to', (host, port), self.source_address)
		return create_connection_af(
			(host, port),
			timeout,
			self.source_address,
			family=socket.AF_INET
		)
