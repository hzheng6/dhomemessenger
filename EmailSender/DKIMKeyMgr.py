#!/usr/bin/env python3
# -*- coding:utf-8 -*-
###
# Copyright (c) 2022 Haofan Zheng
# Use of this source code is governed by an MIT-style
# license that can be found in the LICENSE file or at
# https://opensource.org/licenses/MIT.
###


import email.message
import logging
import os

from typing import Union

import dkim # python3 -m pip install dkimpy[ed25519]


class DKIMKeyMgr(object):

	def __init__(
		self,
		domain: str,
		algorithm: str,
		selector: str,
		keyPath: Union[str, os.PathLike, None] = None,
		keyBytes: Union[bytes, None] = None
	) -> None:
		super(DKIMKeyMgr, self).__init__()

		self.domain = domain
		self.algorithm = algorithm
		self.selector = selector
		self.keyPath = keyPath

		if (keyBytes is not None) and (keyPath is not None):
			raise ValueError(
				'keyBytes and keyPath cannot be both set.'
			)

		if keyBytes is not None:
			self.keyPemBin = keyBytes
		elif keyPath is not None:
			with open(self.keyPath, 'rb') as keyFile:
				self.keyPemBin = keyFile.read()
		else:
			raise ValueError(
				'keyBytes and keyPath cannot be both None.'
			)

		self.logger = logging.getLogger(
			__name__ + '.' + self.__class__.__name__
		)


	def SignEmailMsg(
		self,
		emailMsg: email.message.EmailMessage
	) -> str:

		self.logger.debug(
			'signing email with algo {} selector {}'.format(
				self.algorithm,
				self.selector
			)
		)

		existSignature = emailMsg.get(
			name='DKIM-Signature',
			failobj=None
		)
		if existSignature is not None:
			raise ValueError(
				'Email message already has a DKIM signature.'
			)

		headers = [b'To', b'From', b'Subject']
		signature = dkim.sign(
			message=emailMsg.as_bytes(),
			selector=self.selector.encode(),
			domain=self.domain.encode(),
			privkey=self.keyPemBin,
			signature_algorithm=self.algorithm.encode(),
			include_headers=headers
		)

		signature = signature.decode()

		self.logger.debug('original signature result:\n{}'.format(signature))

		if signature.startswith('DKIM-Signature:'):
			signature = signature.replace('DKIM-Signature:', '')
		signature = signature.strip()
		signature = signature.replace('\r\n', '')
		signature = signature.replace('\r', '')
		signature = signature.replace('\n', '')

		self.logger.debug('signature:\n{}'.format(signature))

		return signature
