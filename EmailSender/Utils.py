#!/usr/bin/env python3
# -*- coding:utf-8 -*-
###
# Copyright (c) 2022 Haofan Zheng
# Use of this source code is governed by an MIT-style
# license that can be found in the LICENSE file or at
# https://opensource.org/licenses/MIT.
###


import dns.resolver # python3 -m pip install dnspython


def ResolveDnsNameAlias(oriAddr: str, cname: str):
	if cname.endswith('.'):
		return cname[:len(cname) - 1]

	# cname is not a FQDN
	# somecname.cname.example.com CNAME target
	# should be resolved to target.example.com
	targetCmp = [x for x in reversed(oriAddr.split('.'))]
	return cname + '.' + targetCmp[1] + '.' + targetCmp[0]


def GetTargetHost(emailAddr: str):
	targetHost = emailAddr.split('@')[1]

	try:
		dnsRes = dns.resolver.resolve(targetHost, 'mx')
		mxData = str(dnsRes[0].exchange)
		targetHost = ResolveDnsNameAlias(targetHost, mxData)
	except:
		pass

	return targetHost

