#!/usr/bin/env python3
# -*- coding:utf-8 -*-
###
# Copyright (c) 2022 Haofan Zheng
# Use of this source code is governed by an MIT-style
# license that can be found in the LICENSE file or at
# https://opensource.org/licenses/MIT.
###


from typing import Union

from EmailSender import EmailSender

EMAIL_SENDER: Union[EmailSender.EmailSender, None] = None

def Init(
	emailSender: EmailSender.EmailSender
) -> None:
	global EMAIL_SENDER

	EMAIL_SENDER = emailSender


def Get(name: str):
	glb = globals()
	val = glb.get(name, None)

	if val is None:
		raise ValueError("Singleton not initialized: " + name)
	else:
		return val
