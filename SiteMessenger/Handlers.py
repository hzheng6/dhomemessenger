#!/usr/bin/env python3
# -*- coding:utf-8 -*-
###
# Copyright (c) 2022 Haofan Zheng
# Use of this source code is governed by an MIT-style
# license that can be found in the LICENSE file or at
# https://opensource.org/licenses/MIT.
###


import base64
import json
import logging
import os
from typing import List, Union

from DHomeHTTPSServer import HttpExceptions
from DHomeHTTPSServer.AuthUtils import PKeyAuth
from DHomeHTTPSServer.Server.HttpRequest import HttpRequest

from EmailSender import EmailSender

from . import Singletons


def _CheckDomain(
	content: dict,
	key: str
) -> str:
	if key not in content:
		raise HttpExceptions.BadRequest(
			'"{}" must be specified'.format(key)
		)

	if not isinstance(content[key], str):
		raise HttpExceptions.BadRequest(
			'"{}" must be a string'.format(key)
		)

	return content[key]


def _CheckEmails(email: str) -> bool:
	if not isinstance(email, str):
		return False
	if email.count('@') != 1:
		return False

	return True


def _CheckRecipients(
	content: dict,
	key: str
) -> Union[List[str], None]:
	if key in content:
		if not isinstance(content[key], list):
			raise HttpExceptions.BadRequest(
				'"{}" must be a list of strings'.format(key)
			)

		if len(content[key]) == 0:
			return None
		else:
			res = []
			for email in content[key]:
				if not _CheckEmails(email):
					raise HttpExceptions.BadRequest(
						'"{}" must be a list of valid emails'.format(key)
					)
				else:
					res.append(email)
			return res
	else:
		return None


def _CheckContent(content: dict, key: str) -> str:
	if key not in content:
		raise HttpExceptions.BadRequest(
			'"{}" must be specified'.format(key)
		)

	if not isinstance(content[key], str):
		raise HttpExceptions.BadRequest(
			'"{}" must be a string'.format(key)
		)

	return content[key]


def _CheckSubject(
	content: dict,
	key: str
) -> Union[str, None]:
	if key not in content:
		return None

	if not isinstance(content[key], str):
		raise HttpExceptions.BadRequest(
			'"{}" must be a string'.format(key)
		)

	return content[key]


def _CheckAttachments(
	content: dict,
	key: str
) -> Union[dict, None]:
	if key not in content:
		return None

	if not isinstance(content[key], list):
		raise HttpExceptions.BadRequest(
			'"{}" must be a list of attachments'.format(key)
		)

	if len(content[key]) == 0:
		return None

	res = []
	for attachment in content[key]:
		if not isinstance(attachment, dict):
			raise HttpExceptions.BadRequest(
				'"{}" must be a list of dictionary'.format(key)
			)

		if (('data' not in attachment) or
			(not isinstance(attachment['data'], str)) or
			('maintype' not in attachment) or
			(not isinstance(attachment['maintype'], str)) or
			('subtype' not in attachment) or
			(not isinstance(attachment['subtype'], str)) or
			('filename' not in attachment) or
			(not isinstance(attachment['filename'], str))):
			raise HttpExceptions.BadRequest(
				'"{}" body is ill-formed'.format(key)
			)

		cleanedAttach = {}
		try:
			cleanedAttach['data'] = base64.b64decode(
				attachment['data'].encode()
			)
			cleanedAttach['data'] = base64.b64encode(
				cleanedAttach['data']
			).decode()
		except Exception:
			raise HttpExceptions.BadRequest(
				'"{}" body is ill-formed'.format(key)
			)
		cleanedAttach['maintype'] = attachment['maintype']
		cleanedAttach['subtype'] = attachment['subtype']
		cleanedAttach['filename'] = attachment['filename']
		res.append(cleanedAttach)

	return res


def SendEmail(req: HttpRequest) -> None:
	'''

# SendEmail

Handle the request to send a email.

The request should be in JSON with the following format:
```json
{
	"domain": "example.com",
	"to": [
		"Optional(EitherToOrBcc)@example.com",
		"ListOfReceiver@example.com",
		...
	],
	"bcc": [
		"Optional(EitherToOrBcc)@example.com",
		"ListOfReceiver@example.com",
		...
	],
	"subject": "(Optional) Subject of the email",
	"content": "Content of the email",
	"attachments": [
		{
			"_Optional": "_Optional",
			"data": "Base64-encoded binary data",
			"maintype": "Main type of the attachment",
			"subtype": "Subtype of the attachment",
			"filename": "Filename of the attachment"
		},
		...
	]
}
```

The response is in the format of:
```json
{
	"success": true
}
```
or:
```json
{
	"success": false,
	"errors": [
		<errors from the smtp.SMTP.send_message() call>
	]
}
```

If error occurs, a non-200 status code will be responded.

## Parameters
- `req` The request object.

## Returns
- `None`

	'''

	logger = logging.getLogger(__name__ + '.' + SendEmail.__name__)

	if req.reqHandler.command != 'POST':
		raise HttpExceptions.MethodNotAllowed()

	logger.debug('Received SendEmail request')

	# get peer cert
	sock = req.reqHandler.GetSock()
	peerCertMgr = PKeyAuth.PeerCertMgr(
		peerCertBytes=sock.getpeercert(binary_form=True)
	)
	fromUsername = peerCertMgr.GetCommonName()

	emailSender: EmailSender.EmailSender = (
		Singletons.Get('EMAIL_SENDER')
	)

	# get request content
	cType, content = req.reqHandler.ReadBodyAutoType()

	if cType != 'json':
		raise HttpExceptions.BadRequest('Content-Type must be application/json')

	# check domain
	domain = _CheckDomain(content, 'domain')
	if domain != emailSender.emailDomain:
		raise HttpExceptions.BadRequest(
			'Domain "{}" is not allowed'.format(domain)
		)

	# check recipients
	hdrTo  = _CheckRecipients(content, 'to')
	hdrBcc = _CheckRecipients(content, 'bcc')
	if (hdrTo is None) and (hdrBcc is None):
		raise HttpExceptions.BadRequest(
			'Either "to" or "bcc" must be specified'
		)

	# check content
	emailContent = _CheckContent(content, 'content')

	# check subject
	hdrSubject = _CheckSubject(content, 'subject')

	# check attachments
	attachments = _CheckAttachments(content, 'attachments')

	sendRes = emailSender.SendEmail(
		fromUsername=fromUsername,
		hdrTo=hdrTo,
		content=emailContent,
		hdrBcc=hdrBcc,
		hdrSubject=hdrSubject,
		attachments=attachments
	)

	respBody = {}
	if sendRes is None:
		respBody['success'] = True
	else:
		respBody['success'] = False
		respBody['errors'] = sendRes

	req.reqHandler.RespAutoType('json', respBody)
