#!/usr/bin/env python3
# -*- coding:utf-8 -*-
###
# Copyright (c) 2022 Haofan Zheng
# Use of this source code is governed by an MIT-style
# license that can be found in the LICENSE file or at
# https://opensource.org/licenses/MIT.
###


from DHomeHTTPSServer.Server.UriUtils import UriPair

from .Handlers import SendEmail

URL_MAP = [
	UriPair('send_email/', SendEmail),
]
