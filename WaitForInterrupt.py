#!/usr/bin/env python3
# -*- coding:utf-8 -*-
###
# Copyright (c) 2022 Haofan Zheng
# Use of this source code is governed by an MIT-style
# license that can be found in the LICENSE file or at
# https://opensource.org/licenses/MIT.
###


import logging
import signal
import time


class WaitForInterrupt(object):

	def __init__(self, updInterval: float = 0.5):

		super(WaitForInterrupt, self).__init__()

		self.logger = logging.getLogger(
			__name__ + '.' + self.__class__.__name__)

		self.received = None
		self.updInterval = updInterval


	def Handler(self, sigNum: int, frame: object):
		sigRecvName = signal.Signals(sigNum).name
		self.logger.info('Received signal: {}'.format(sigRecvName))
		self.received = sigNum


	def RegisterHandler(self):
		sigSet = [signal.SIGINT, signal.SIGTERM]
		if getattr(signal, 'SIGBREAK', None):
			sigSet.append(signal.SIGBREAK)

		# Register signal handler
		for sigNum in sigSet:
				signal.signal(sigNum, self.Handler)


	def IsSignalReceived(self) -> bool:
		return self.received is not None


	def Run(self):
		self.RegisterHandler()

		# begin to wait for signals
		self.logger.info('Waiting for interrupt/termination signal')
		while not self.IsSignalReceived():
			time.sleep(self.updInterval)
